FROM fluent/fluentd:v1.15
USER root
RUN ["gem", "install", "fluent-plugin-elasticsearch", "--no-document", "--version", "5.2.4"]
RUN ["gem", "install", "fluent-plugin-multi-format-parser"]
USER fluent
